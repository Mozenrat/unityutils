﻿using UnityEditor;
using UnityEngine;

namespace Essentials.MeshHexGrid.Editor
{
    [CustomEditor(typeof(MeshHexGridMap))]
    public class MeshHexGridMapEditor : UnityEditor.Editor
    {
        private MeshHexGridMap thisMap;
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            
            thisMap = (MeshHexGridMap) target;

            if (GUILayout.Button("Draw HexMap"))
            {
                thisMap.BuildHexGrid();
            }
            if (GUILayout.Button("Draw Down Strip"))
            {
                thisMap.BuildHexStripDown();
            }
        }
    }
}