﻿using Essentials.ProceduralHexGrid;
using UnityEngine;

namespace ProceduralGridEditor
{
    [UnityEditor.CustomEditor(typeof(HexGridMap))]
    public class HexGridRendererEditor : UnityEditor.Editor
    {
        private HexGridMap thisMap;
        private bool redrawConstantly; 
        
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            thisMap = (HexGridMap) target;
            redrawConstantly = GUILayout.Toggle(redrawConstantly, "Redraw Constantly");
            if (redrawConstantly)
            {
                thisMap.DrawGridMap();
            }

            if (GUILayout.Button("Draw HexMap"))
            {
                thisMap.DrawGridMap();
            }
        }
    }
}