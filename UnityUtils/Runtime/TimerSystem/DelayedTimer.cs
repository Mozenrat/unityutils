﻿using System;
using System.Collections;
using UnityEngine;

namespace Essentials.TimerSystem
{
    public class DelayedTimer
    {
        private readonly Action onBeforeTick;
        private readonly Action onTick;
        private readonly Action onKill;
        private readonly WaitForSeconds delay;
        private Action onAfterTick;
        
        private bool disabled;
        
        public DelayedTimer(
            float delayInSeconds,
            Action onTick,
            Action onKill = null,
            Action onBeforeTick = null,
            Action onAfterTick = null)
        {
            delay = new WaitForSeconds(delayInSeconds);
            this.onTick = onTick;
            this.onKill = onKill;
            this.onBeforeTick = onBeforeTick;
            this.onAfterTick = onAfterTick;
        }
        
        public IEnumerator Tick()
        {
            yield return delay;

            if (disabled) yield break;
            
            onBeforeTick?.Invoke();
            onTick.Invoke();
            onAfterTick?.Invoke();
        }

        public void InjectAfterTick(Action onAfterTickInjected)
        {
            onAfterTick = onAfterTickInjected;
        }

        public void Kill()
        {
            disabled = true;
            onKill?.Invoke();
        }
    }
}