﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Essentials.TimerSystem
{
    public class TimerHost : MonoBehaviour
    {
        private static TimerHost _instance;
        
        private List<RecurringTimer> activeRecurringTimers;
        private List<NextFrameTimer> activeNextFrameTimers;
        private List<DelayedTimer> activeDelayedActionTimers;
        
        public static RecurringTimer NewRecurringTimer(float interval, Action onTick, Action onKill = null, Action onBeforeTick = null, Action onAfterTick = null)
        {
            var newTimer = new RecurringTimer(interval, onTick, onKill, onBeforeTick, onAfterTick);
            _instance.activeRecurringTimers.Add(newTimer);

            return newTimer;
        }

        public static NextFrameTimer NewNextFrameTimer(Action onTick, Action onKill = null, Action onBeforeTick = null, Action onAfterTick = null)
        {
            var nextFrameTimer = new NextFrameTimer(onTick, onKill, onBeforeTick, onAfterTick);
            _instance.activeNextFrameTimers.Add(nextFrameTimer);
            _instance.StartCoroutine(nextFrameTimer.Tick());
            return nextFrameTimer;
        }

        public static DelayedTimer NewDelayedTimer(float delayInSeconds, Action onTick, Action onKill = null, Action onBeforeTick = null, Action onAfterTick = null)
        {
            var delayedTimer = new DelayedTimer(delayInSeconds, onTick, onKill, onBeforeTick, onAfterTick);
            _instance.activeDelayedActionTimers.Add(delayedTimer);
            _instance.StartCoroutine(delayedTimer.Tick());
            return delayedTimer;
        }

        public static void ExecuteOnceDelayedTimer(float delayInSeconds, Action onTick)
        {
            var delayedTimer = new DelayedTimer(delayInSeconds, onTick);
            delayedTimer.InjectAfterTick(() => KillTimer(delayedTimer));
            
            _instance.activeDelayedActionTimers.Add(delayedTimer);
            _instance.StartCoroutine(delayedTimer.Tick());
        }
        
        public static void ExecuteOnceNextFrameTimer(Action onTick)
        {
            var nextFrameTimer = new NextFrameTimer(onTick);
            nextFrameTimer.InjectAfterTick(() => KillTimer(nextFrameTimer));
            
            _instance.activeNextFrameTimers.Add(nextFrameTimer);
            _instance.StartCoroutine(nextFrameTimer.Tick());
        }

        public static void KillTimer(RecurringTimer recurringTimer)
        {
            if (recurringTimer == null) return;
            _instance.activeRecurringTimers.Remove(recurringTimer);
            recurringTimer.Kill();
        }
        
        public static void KillTimer(NextFrameTimer nextFrameTimer)
        {
            if (nextFrameTimer == null) return;
            _instance.activeNextFrameTimers.Remove(nextFrameTimer);
            nextFrameTimer.Kill();
        }

        public static void KillTimer(DelayedTimer delayedTimer)
        {
            if (delayedTimer == null) return;
            _instance.activeDelayedActionTimers.Remove(delayedTimer);
            delayedTimer.Kill();
        }

        private void Awake()
        {
            activeRecurringTimers = new List<RecurringTimer>();
            activeNextFrameTimers = new List<NextFrameTimer>();
            activeDelayedActionTimers = new List<DelayedTimer>();
            if (_instance == null) _instance = this;
        }

        private void Update()
        {
            for (int i = 0; i < activeRecurringTimers.Count; i++)
            {
                activeRecurringTimers[i].Tick(Time.deltaTime);
            }
        }

        private void OnDisable()
        {
            foreach (var timer in activeRecurringTimers)
            {
                timer.Kill();
            }
            foreach (var timer in activeNextFrameTimers)
            {
                timer.Kill();
            }
            foreach (var timer in activeDelayedActionTimers)
            {
                timer.Kill();
            }
            activeRecurringTimers.Clear();
            activeNextFrameTimers.Clear();
            activeDelayedActionTimers.Clear();
            _instance.StopAllCoroutines();
        }
    }
}