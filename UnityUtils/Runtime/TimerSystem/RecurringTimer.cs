﻿using System;

namespace Essentials.TimerSystem
{
    public class RecurringTimer
    {
        private readonly float interval;
        private readonly Action onBeforeTick;
        private readonly Action onAfterTick;
        private readonly Action onTick;
        private readonly Action onKill;

        private float intervalCurrent;
        
        public RecurringTimer(
            float interval,
            Action onTick,
            Action onKill = null,
            Action onBeforeTick = null,
            Action onAfterTick = null)
        {
            this.interval = interval;
            this.onTick = onTick;
            this.onKill = onKill;
            this.onBeforeTick = onBeforeTick;
            this.onAfterTick = onAfterTick;
        }

        public void Tick(float timeIncrement)
        {
            intervalCurrent += timeIncrement;
            if (intervalCurrent >= interval)
            {
                onBeforeTick?.Invoke();
                intervalCurrent = 0f;
                onTick.Invoke();
            }
            onAfterTick?.Invoke();
        }

        public void Kill()
        {
            onKill?.Invoke();
        }
    }
}