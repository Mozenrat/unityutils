﻿namespace Essentials.TimerSystem
{
    public static class TimerExtensions
    {
        public static void Kill(this RecurringTimer timer)
        {
            TimerHost.KillTimer(timer);
        }
        
        public static void Kill(this NextFrameTimer timer)
        {
            TimerHost.KillTimer(timer);
        }
        
        public static void Kill(this DelayedTimer timer)
        {
            TimerHost.KillTimer(timer);
        }
    }
}