﻿using UnityEngine;

namespace Essentials.DependencyInjection
{
    public class InjectableMonoBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Make sure to call base.Awake() in override if you need awake.
        /// </summary>
        protected virtual void Awake()
        {
            Injector.Instance.ResolveInjectable(this);
        }
    }
}