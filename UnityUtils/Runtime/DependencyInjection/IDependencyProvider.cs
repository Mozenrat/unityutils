using System;
using UnityEngine;

namespace Essentials.DependencyInjection
{
    public interface IDependencyProvider { }

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class InjectAttribute : PropertyAttribute
    {
        public readonly string Id;
        
        public InjectAttribute(string id = "")
        {
            Id = id;
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ProvideAttribute : PropertyAttribute
    {
        public readonly string Id;
        
        public ProvideAttribute(string id = "")
        {
            Id = id;
        }
    }
}