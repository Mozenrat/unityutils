﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Essentials.Singletons;
using UnityEngine;

namespace Essentials.DependencyInjection
{
    [DefaultExecutionOrder(-90)]
    public class Injector : Singleton<Injector>
    {
        private const BindingFlags ProviderBindingFlags = BindingFlags.Instance |
                                                          BindingFlags.Public |
                                                          BindingFlags.Static |
                                                          BindingFlags.FlattenHierarchy;

        private const BindingFlags InjectableBindingFlags = BindingFlags.Instance |
                                                            BindingFlags.Public |
                                                            BindingFlags.NonPublic |
                                                            BindingFlags.FlattenHierarchy;

        readonly Dictionary<Type, Dictionary<string, object>> instanceRegistry = new();
        
        public int InjectionInheritanceDepth { get; set; } = 2;

        public void ResolveProvider(object provider)
        {
            if (provider is IDependencyProvider dependencyProvider) RegisterProvider(dependencyProvider);
        }

        public void ResolveInjectable(object injectable)
        {
            if (IsInjectableRecursive(injectable.GetType(), InjectionInheritanceDepth)) InjectDependencies(injectable);
        }

        private void InjectDependencies(object injectableInstance)
        {
            var type = injectableInstance.GetType();

            // Inject into fields
            var injectableFields = GetInjectableFieldsRecursive(type, InjectionInheritanceDepth);

            foreach (var injectableField in injectableFields)
            {
                if (injectableField.GetValue(injectableInstance) != null)
                {
                    Debug.LogWarning($"[Injector] Field '{injectableField.Name}' of class '{type.Name}' is already set."); continue;
                }

                var fieldType = injectableField.FieldType;
                var stringId = injectableField.GetCustomAttribute<InjectAttribute>().Id;
                var resolvedInstance = ResolveInstance(fieldType, stringId);
                
                if (resolvedInstance == null)
                {
                    throw new Exception($"Failed to inject dependency into field '{fieldType.Name}' of class '{type.Name}' the resolved instance is null.");
                }

                injectableField.SetValue(injectableInstance, resolvedInstance);
            }
        }

        private void RegisterProvider(IDependencyProvider provider)
        {
            // Register provided instances from properties
            var properties = provider.GetType().GetProperties(ProviderBindingFlags);
            foreach (var property in properties)
            {
                if (!Attribute.IsDefined(property, typeof(ProvideAttribute))) continue;

                var stringId = property.GetCustomAttribute<ProvideAttribute>().Id;
                var propertyType = property.PropertyType;
                var providedInstance = property.GetValue(provider);
                if (providedInstance != null)
                {
                    // If registry already contains the property type, add the provided instance to the dictionary
                    if (instanceRegistry.TryGetValue(propertyType, out var objectDict))
                    {
                        objectDict.Add(stringId, providedInstance);
                    }
                    // If registry doesn't contain the property type, add the property type and the provided concrete instance to the dictionary
                    else
                    {
                        instanceRegistry.Add(propertyType,
                            new Dictionary<string, object> { { stringId, providedInstance } });
                    }
                }
                else
                {
                    throw new Exception($"Provider property '{property.Name}' in class '{provider.GetType().Name}' returned null when providing type '{propertyType.Name}'.");
                }
            }
        }
        
        private IEnumerable<FieldInfo> GetInjectableFieldsRecursive(Type type, int depth)
        {
            if (depth == 0) return Enumerable.Empty<FieldInfo>();
            var injectableFields = GetInjectableFields(type);
            return injectableFields.Concat(GetInjectableFieldsRecursive(type.BaseType, depth - 1));
        }
        
        private IEnumerable<FieldInfo> GetInjectableFields(Type type)
        {
            return type.GetFields(InjectableBindingFlags)
                .Where(member => Attribute.IsDefined(member, typeof(InjectAttribute)));
        }

        private void ResolveExistingProviders()
        {
            var allExistingProviders = FindProviders();
            foreach (var provider in allExistingProviders)
            {
                RegisterProvider(provider);
            }
        }

        private IEnumerable<IDependencyProvider> FindProviders()
        {
            return FindObjectsByType<MonoBehaviour>(FindObjectsSortMode.None).OfType<IDependencyProvider>();
        }

        private object ResolveInstance(Type type, string stringId = "")
        {
            instanceRegistry.TryGetValue(type, out var resolvedDictionary);
            if (resolvedDictionary == null) return null;
            
            if (stringId == "")
            {
                // If no concrete type was specified during provision there should only be one instance in the dictionary
                return resolvedDictionary.Values.SingleOrDefault();
            }
            
            resolvedDictionary.TryGetValue(stringId, out var resolvedInstance);
            return resolvedInstance;
        }
        
        private bool IsInjectableRecursive(Type type, int depth)
        {
            if (depth == 0) return false;
            var injectableFields = GetInjectableFields(type);
            if (injectableFields.Any()) return true;
            return IsInjectableRecursive(type.BaseType, depth - 1);
        }
    }
}