﻿namespace Essentials.StateMachine
{
    public interface IStateTransition
    {
        IState To { get; }
        ITransitionPredicate Condition { get; }
    }
}