﻿namespace Essentials.StateMachine
{
    public interface IState
    {
        void TickState();
        void EnterState();
        void ExitState();
        bool MustTransitionState();
    }
}