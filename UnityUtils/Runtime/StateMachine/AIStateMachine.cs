﻿using System;
using System.Collections.Generic;
using Essentials.TimerSystem;

namespace Essentials.StateMachine
{
    public class AIStateMachine {
        private StateNode current;
        private readonly Dictionary<Type, StateNode> stateNodes = new();
        private readonly HashSet<IStateTransition> anyTransitions = new();
        
        public IState PeekActiveState => current.State;

        // starting state will start ticking on the next frame
        public AIStateMachine(List<IState> possibleStates, IState startingState) {
            foreach (var state in possibleStates)
                GetOrAddNode(state);
            
            current = stateNodes[startingState.GetType()];
            TimerHost.ExecuteOnceNextFrameTimer(() => current.State.EnterState());
        }
        
        // starting state will not be set, so it will not tick until SetState is called
        public AIStateMachine(List<IState> possibleStates) {
            foreach (var state in possibleStates)
                GetOrAddNode(state);
        }

        public void TickStates() {
            var transition = GetTransition();
            if (transition != null) 
                ChangeState(transition.To);

            current.State.TickState();
        }

        public void SetState(IState state) {
            ChangeState(state);
        }
        
        public void AddTransition(IState from, IState to, ITransitionPredicate condition) {
            GetOrAddNode(from).AddTransition(GetOrAddNode(to).State, condition);
        }
        
        public void AddAnyTransition(IState to, ITransitionPredicate condition) {
            anyTransitions.Add(new StateTransition(GetOrAddNode(to).State, condition));
        }

        private void ChangeState(IState state) {
            if (state == current?.State) return;
            
            var previousState = current?.State;
            var nextState = stateNodes[state.GetType()].State;
            
            previousState?.ExitState();
            nextState.EnterState();
            current = stateNodes[state.GetType()];
        }

        private IStateTransition GetTransition() {
            foreach (var transition in anyTransitions)
                if (transition.Condition.Evaluate())
                    return transition;
            
            foreach (var transition in current.Transitions)
                if (transition.Condition.Evaluate())
                    return transition;
            
            return null;
        }

        private StateNode GetOrAddNode(IState state) {
            var node = stateNodes.GetValueOrDefault(state.GetType());
            
            if (node == null) {
                node = new StateNode(state);
                stateNodes.Add(state.GetType(), node);
            }
            
            return node;
        }

        private class StateNode {
            public IState State { get; }
            public HashSet<IStateTransition> Transitions { get; }
            
            public StateNode(IState state) {
                State = state;
                Transitions = new HashSet<IStateTransition>();
            }
            
            public void AddTransition(IState to, ITransitionPredicate condition) {
                Transitions.Add(new StateTransition(to, condition));
            }
        }
    }
}