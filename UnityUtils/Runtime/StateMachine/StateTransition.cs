﻿namespace Essentials.StateMachine
{
    public class StateTransition : IStateTransition
    {
        public IState To { get; }
        public ITransitionPredicate Condition { get; }
        
        public StateTransition(IState to, ITransitionPredicate condition) {
            To = to;
            Condition = condition;
        }
    }
}