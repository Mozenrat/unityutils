﻿using System;

namespace Essentials.StateMachine
{
    public class FuncPredicate : ITransitionPredicate
    {
        private readonly Func<bool> func;
        
        public FuncPredicate(Func<bool> func) {
            this.func = func;
        }
        
        public bool Evaluate() => func.Invoke();
    }
}