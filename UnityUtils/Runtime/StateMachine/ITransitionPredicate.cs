﻿namespace Essentials.StateMachine
{
    public interface ITransitionPredicate
    {
        bool Evaluate();
    }
}