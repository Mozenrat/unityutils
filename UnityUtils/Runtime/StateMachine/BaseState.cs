﻿using System;

namespace Essentials.StateMachine
{
    public abstract class BaseState : IState
    {
        public virtual void TickState() { }
        public virtual void EnterState() { }
        public virtual void ExitState() { }
        public virtual bool MustTransitionState() => false;
    }
}