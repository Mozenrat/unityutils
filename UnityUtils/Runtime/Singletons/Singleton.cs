﻿using UnityEngine;

namespace Essentials.Singletons
{
    public class Singleton<T> : MonoBehaviour where T : Component {
        protected static T instance;

        public static T Instance {
            get {
                if (instance == null) {
                    instance = FindAnyObjectByType<T>();
                    if (!Application.isPlaying || Application.exitCancellationToken.IsCancellationRequested) return instance;
                    if (instance == null) {
                        var go = new GameObject($"[Singleton] {typeof(T).Name} Auto-Generated");
                        go.transform.SetParent(GetSingletonsParent());
                        instance = go.AddComponent<T>();
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Make sure to call base.Awake() in override if you need awake.
        /// </summary>
        protected virtual void Awake() {
            InitializeSingleton();
        }

        protected virtual void InitializeSingleton() {
            if (!Application.isPlaying) return;

            if (instance == null) {
                instance = this as T;
                DontDestroyOnLoad(gameObject);
            } else {
                if (instance != this) {
                    Destroy(gameObject);
                }
            }
        }
        
        private static Transform GetSingletonsParent() {
            var parent = GameObject.Find("Singletons");
            if (parent == null) {
                parent = new GameObject("Singletons");
                DontDestroyOnLoad(parent);
            }

            return parent.transform;
        }
    }
}