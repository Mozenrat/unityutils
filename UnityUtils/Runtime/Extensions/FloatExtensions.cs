﻿using UnityEngine;

namespace Essentials.Extensions
{
    public static class FloatExtensions
    {
        public static string RoundToIntString(this float value)
        {
            return string.Format($"{Mathf.RoundToInt(value)}");
        }
    }
}