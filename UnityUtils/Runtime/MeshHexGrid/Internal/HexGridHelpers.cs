﻿using UnityEngine;

namespace Essentials.MeshHexGrid.Internal
{
    public static class HexGridHelpers
    {
        public static Vector3 CalcHexPositionFromCoordinate(int inputXPosition, int inputYPosition, float hexTileSize = 1f, bool isFlatTopped = false)
        {
            var column = inputXPosition;
            var row = inputYPosition;

            float width;
            float height;
            float xPos;
            float yPos;
            bool shouldOffset;
            float horizontalDistance;
            float verticalDistance;
            float offset;
            float size = hexTileSize;

            if (!isFlatTopped)
            {
                shouldOffset = row % 2 == 0;
                width = 1.73205f * size; // Sqrt(3)
                height = 2f * size;

                horizontalDistance = width;
                verticalDistance = height * .75f;
                offset = shouldOffset ? width * .5f : 0f;

                xPos = column * horizontalDistance + offset;
                yPos = row * verticalDistance;
            }
            else
            {
                shouldOffset = column % 2 == 0;
                width = 2f * size;
                height = 1.73205f * size; // Sqrt(3)

                horizontalDistance = width * .75f;
                verticalDistance = height;
                offset = shouldOffset ? height * .5f : 0f;
                
                xPos = column * horizontalDistance;
                yPos = row * verticalDistance - offset;
            }

            return new Vector3(xPos, 0f, -yPos);
        }
    }
}