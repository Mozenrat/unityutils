﻿namespace Essentials.MeshHexGrid.Internal
{
    public enum HexTileType
    {
        Uninitialized = 0,
        Ground = 1,
        Water = 2,
        Mountain = 3,
    }
}