﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Essentials.MeshHexGrid.Internal
{
    [ExecuteInEditMode]
    public class MeshHexTile : MonoBehaviour
    {
        [SerializeField] private HexTileType TileType;
        
        private MeshHexTileSettings tileSettings;
        private GameObject tileInstance;

        public void Init(MeshHexTileSettings settings)
        {
            tileSettings = settings;
        }

        public void SetRandomTileType()
        {
            var totalEnumTypesCount = Enum.GetValues(typeof(HexTileType)).Length;
            TileType = (HexTileType) Random.Range(1, totalEnumTypesCount);
        }

        public void InstantiateTileObject()
        {
            var prefabRef = tileSettings.GetPrefab(TileType);
            tileInstance = Instantiate(prefabRef, transform);
        }

#if UNITY_EDITOR
        private bool isDirty;

        private void OnValidate()
        {
            if (!tileInstance || !tileSettings) return;
            isDirty = true;
        }

        private void Update()
        {
            if (!isDirty) return;
            
            if (Application.isPlaying)
            {
                Destroy(tileInstance);
            }
            else
            {
                DestroyImmediate(tileInstance);
            }

            InstantiateTileObject();
            isDirty = false;
        }
#endif
    }
}