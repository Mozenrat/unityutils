﻿using Essentials.MeshHexGrid.Internal;
using UnityEngine;

namespace Essentials.MeshHexGrid
{
    [CreateAssetMenu(menuName = "HexGrid/HexTileSettings")]
    public class MeshHexTileSettings : ScriptableObject
    {
        public GameObject GroundTilePrefab;
        public GameObject WaterTilePrefab;
        public GameObject MountainTilePrefab;

        public GameObject GetPrefab(HexTileType tileType)
        {
            switch (tileType)
            {
                case HexTileType.Ground:
                    return GroundTilePrefab;
                case HexTileType.Water:
                    return WaterTilePrefab;
                case HexTileType.Mountain:
                    return MountainTilePrefab;
                default:
                    return null;
            }
        }
    }
}