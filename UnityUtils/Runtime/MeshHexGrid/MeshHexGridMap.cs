﻿using System.Collections.Generic;
using Essentials.MeshHexGrid.Internal;
using UnityEngine;

namespace Essentials.MeshHexGrid
{
    public class MeshHexGridMap : MonoBehaviour
    {
        [SerializeField] private Vector2Int GridSize;
        
        [SerializeField] private MeshHexTileSettings TileSettings;

        public void ClearGrid()
        {
            var children = new List<GameObject>();

            for (int i = 0; i < transform.childCount; i++)
            {
                children.Add(transform.GetChild(i).gameObject);
            }

            foreach (var child in children)
            {
                DestroyImmediate(child);
            }
        }

        public void BuildHexGrid()
        {
            ClearGrid();

            for (int x = 0; x < GridSize.x; x++)
            {
                for (int y = 0; y < GridSize.y; y++)
                {
                    var tilePosition = HexGridHelpers.CalcHexPositionFromCoordinate(x, y);
                    var newTile = new GameObject($"HexTile {x}, {y}");
                    var hexTileComponent = newTile.AddComponent<MeshHexTile>();
                    hexTileComponent.Init(TileSettings);
                    hexTileComponent.SetRandomTileType();
                    hexTileComponent.InstantiateTileObject();
                    newTile.transform.SetPositionAndRotation(tilePosition, Quaternion.identity);
                    newTile.transform.SetParent(transform);
                }
            }
        }

        public void BuildHexStripDown()
        {
            ClearGrid();

            int x = GridSize.x;
            int yCount = GridSize.y;
            while (x >= 0)
            {
                for (int y = 0; y < yCount; y++)
                {
                    var tilePosition = HexGridHelpers.CalcHexPositionFromCoordinate(x, y);
                    var newTile = new GameObject($"HexTile {x}, {y}");
                    var hexTileComponent = newTile.AddComponent<MeshHexTile>();
                    hexTileComponent.Init(TileSettings);
                    hexTileComponent.SetRandomTileType();
                    hexTileComponent.InstantiateTileObject();
                    newTile.transform.SetPositionAndRotation(tilePosition, Quaternion.identity);
                    newTile.transform.SetParent(transform);
                }

                x--;
                yCount--;
            }
        }
    }
}