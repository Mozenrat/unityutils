using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Essentials.Pooling
{
    public static class EntityPool
    {
        private static Dictionary<GameObject, Pool> _pools;
        private static readonly StringBuilder NameBuilder = new();
        private const int DefaultPoolSize = 20;

        private static void InitPool(GameObject prefab, int poolSize = DefaultPoolSize, Transform objectParent = null)
        {
            _pools ??= new Dictionary<GameObject, Pool>();

            if (prefab != null && !_pools.ContainsKey(prefab))
            {
                Pool pool = new Pool(prefab, poolSize, NameBuilder, objectParent);
                _pools.Add(prefab, pool);
            }
        }

        public static void PreloadEntities(GameObject prefab, int poolSize, Transform objectParent = null)
        {
            InitPool(prefab, poolSize, objectParent);
        }

        public static GameObject InstantiateEntity(GameObject prefab, Vector3 position, Quaternion rotation,
            Transform objectParent, int layer = -1)
        {
            InitPool(prefab);
            return _pools[prefab].Spawn(position, rotation, objectParent, layer);
        }

        public static T InstantiateEntity<T>(T prefab, Vector3 position, Quaternion rotation, Transform objectParent, int layer = -1)
            where T : MonoBehaviour
        {
            InitPool(prefab.gameObject);
            return _pools[prefab.gameObject].Spawn(position, rotation, objectParent, layer).GetComponent<T>();
        }

        public static void ReclaimEntity(GameObject instance)
        {
            if (!instance) return;
            var poolMember = instance.GetComponent<PoolMemberComponent>();
            if (poolMember == null)
            {
                Object.Destroy(instance);
            }
            else
            {
                poolMember.AssignedPool.Despawn(instance);
            }
        }

        public static void ReleaseAllPools()
        {
            foreach (var pool in _pools)
            {
                pool.Value.Destroy();
            }

            _pools.Clear();
        }

        internal class Pool
        {
            private int poolMemberId;

            private readonly Queue<GameObject> pooledObjects;
            private readonly GameObject prefabRef;
            private readonly StringBuilder stringBuilder;

            private Transform objectParent;

            public Pool(GameObject prefab, int poolSize, StringBuilder stringBuilder, Transform objectParent = null)
            {
                prefabRef = prefab;
                pooledObjects = new Queue<GameObject>(poolSize);
                this.stringBuilder = stringBuilder;
                this.objectParent = objectParent;
                poolMemberId = 0;

                PrePopulatePool(poolSize);
            }

            private void PrePopulatePool(int size)
            {
                for (int i = 0; i < size; i++)
                {
                    var obj = InstantiateNewObject(Vector3.zero, Quaternion.identity, objectParent);
                    obj.SetActive(false);
                    pooledObjects.Enqueue(obj);
                }
            }

            public GameObject Spawn(Vector3 position, Quaternion rotation, Transform parent, int layer = -1)
            {
                var obj = pooledObjects.Count == 0
                    ? InstantiateNewObject(position, rotation, parent, layer)
                    : pooledObjects.Dequeue();

                if (obj == null)
                {
                    // if a parent scene of pooled object was unloaded the unmanaged reference would be lost, going into recursion in this case
                    return Spawn(position, rotation, parent, layer);
                }

                obj.transform.SetPositionAndRotation(position, rotation);
                if (layer != -1) obj.layer = layer;
                obj.SetActive(true);
                return obj;
            }

            public void Despawn(GameObject instance)
            {
                instance.SetActive(false);
                pooledObjects.Enqueue(instance);
            }

            public void Destroy()
            {
                while (pooledObjects.Count > 0)
                {
                    var instance = pooledObjects.Dequeue();
                    Object.Destroy(instance);
                }
            }

            private GameObject InstantiateNewObject(Vector3 position, Quaternion rotation, Transform parent, int layer = -1)
            {
                if (objectParent == null)
                {
                    objectParent = parent;
                }

                var obj = Object.Instantiate(prefabRef, position, rotation, objectParent);
                if (layer != -1) obj.layer = layer;
                obj.AddComponent<PoolMemberComponent>().AssignedPool = this;
                stringBuilder.Clear();
                stringBuilder.Append(prefabRef.name).Append('_').Append(poolMemberId++);
                obj.name = stringBuilder.ToString();
                obj.SetActive(true);
                return obj;
            }
        }

        internal class PoolMemberComponent : MonoBehaviour
        {
            public Pool AssignedPool;
        }
    }
}