﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Essentials.ProceduralSquareGrid
{
    public class SquareGridMap
    {
        private int width;
        private int height;

        private float cellSize;
        private Vector3 originPoint;

        private Vector3[,] gridArray;

        public SquareGridMap(int width, int height, Vector3 originPoint, float cellSize, Action<Vector3> onGridCellCreated)
        {
            this.width = width;
            this.height = height;
            this.cellSize = cellSize;
            this.originPoint = originPoint;

            gridArray = new Vector3[this.width, this.height];

            for (int x = 0; x < this.width; x++)
            {
                for (int y = 0; y < this.height; y++)
                {
                    gridArray[x, y] = new Vector3(this.originPoint.x + x * this.cellSize, 0f, this.originPoint.z + y * this.cellSize);
                    
                    if (IsInsideGrid(x, y)) onGridCellCreated?.Invoke(GetGridCellWorldPosition(x, y));
                }
            }
        }

        public Mesh GenerateGridMesh()
        {
            var verts = new List<Vector3>();
            var uvs = new List<Vector2>();
            var tris = new List<int>();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    verts.Add(gridArray[x, y]);
                    uvs.Add(new Vector2((float) x / (width - 1), (float) y / (height - 1)));
                }
            }

            // TODO: simplify to initial loops
            for (int i = 0; i < verts.Count; i++)
            {
                if ((i + 1) % width == 0 || i + width >= verts.Count) continue;

                tris.Add(i);
                tris.Add(i + width);
                tris.Add(i + 1);
                tris.Add(i + 1);
                tris.Add(i + width);
                tris.Add(i + width + 1);
            }

            var mesh = new Mesh
            {
                name = "SquareGridMesh",
                vertices = verts.ToArray(),
                uv = uvs.ToArray(),
                triangles = tris.ToArray(),
            };
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            return mesh;
        }

        public Vector3? GetCenterOfGridCell(Vector3 worldPos)
        {
            var pointDiff = worldPos - originPoint;
            var xPos = Mathf.FloorToInt(pointDiff.x / cellSize);
            var yPos = Mathf.FloorToInt(pointDiff.z / cellSize);

            return IsInsideGrid(xPos, yPos) ? GetGridCellWorldPosition(xPos, yPos) : null;
        }

        private bool IsInsideGrid(int x, int y)
        {
            return x < width - 1 && x >= 0 && y < height - 1 && y >= 0;
        }

        private Vector3 GetGridCellWorldPosition(int x, int y)
        {
            return gridArray[x, y] + new Vector3(cellSize * .5f, 0f, cellSize * .5f);
        }
    }
}