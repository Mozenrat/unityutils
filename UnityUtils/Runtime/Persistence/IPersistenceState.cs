﻿using System.Diagnostics.CodeAnalysis;

namespace Essentials.Persistence
{
    /// <summary>
    /// Specific state of IPersistenceObject that should be saved to disk
    /// Inherit from this interface on a class that represents a simple state
    ///
    /// e.g:
    /// [Serializable]
    /// public class CharacterListPersistenceState : IPersistenceState
    /// {
    ///     public List<CharacterPersistenceState> AllCharactersState;
    ///     
    ///     public CharacterListPersistenceState(List<PlayerCharacter> playerCharacters)
    ///     {
    ///         AllCharactersState = new List<CharacterPersistenceState>();
    /// 
    ///         foreach (var character in playerCharacters)
    ///         {
    ///             AllCharactersState.Add(new CharacterPersistenceState(character));
    ///         }
    ///     }
    /// }
    /// </summary>
    [SuppressMessage("ReSharper", "InvalidXmlDocComment")]
    public interface IPersistenceState
    {
        
    }
}