﻿namespace Essentials.Persistence
{
    /// <summary>
    /// Interface to use for an object that has any data that needs to be saved to file
    /// Object must be a monobehaviour and be present in the active scene
    /// </summary>
    public interface IPersistenceObject
    {
        /// <summary>
        /// Called to gather current data that needs to be saved,
        /// method implementation needs to fire PersistenceEventsChannel.InvokeOnSendData()
        /// </summary>
        void InvokeOnSendData();
        
        /// <summary>
        /// Called to inject data from file into current object
        /// </summary>
        void InjectPersistenceData(IPersistenceState restoredState);
    }
}