﻿using System;
using System.Collections.Generic;
using System.IO;
using Essentials.ChannelSystem;
using Essentials.TimerSystem;
using OdinSerializer;
using UnityEngine;

namespace Essentials.Persistence
{
    public class PersistenceManager : MonoBehaviour
    {
        private PersistenceEventsChannel persistenceEventsChannel;

        private string GetSavePath => Path.Combine(Application.persistentDataPath, "SaveData");
        
        private const float AutoSaveInterval = 180f;

        private Dictionary<Type, IPersistenceState> persistenceCache;

        private void Start()
        {
            persistenceEventsChannel = DataIndex.GetChannel<PersistenceEventsChannel>();
            
            persistenceCache = new Dictionary<Type, IPersistenceState>();
            
            persistenceEventsChannel.OnSendData += OnSendDataReceived;

            persistenceEventsChannel.OnSaveGameData += PerformSave;
            persistenceEventsChannel.OnLoadGameData += LoadData;
            
            TimerHost.NewRecurringTimer(AutoSaveInterval, PerformSave);
        }

        private void OnSendDataReceived(object sender, PersistenceEventArgs eventArgs)
        {
            persistenceCache.Add(eventArgs.PersistenceObject.Key, eventArgs.PersistenceObject.Value);
        }

        private void PerformSave()
        {
            // TODO: make this asynchronous
            persistenceCache.Clear();
            persistenceEventsChannel.InvokeOnGatherData();
            SaveData();
        }
        
        private void InjectData(Dictionary<Type, IPersistenceState> restoredData)
        {
            foreach (var dataPoint in restoredData)
            {
                var activeObject = FindObjectOfType(dataPoint.Key); // TODO cache all of these
                ((IPersistenceObject) activeObject).InjectPersistenceData(dataPoint.Value);
            }
        }

        private void SaveData()
        {
            var bytes = SerializationUtility.SerializeValue(persistenceCache, DataFormat.JSON);
            File.WriteAllBytes(GetSavePath, bytes);
        }
        
        private void LoadData()
        {
            var readBytes = File.ReadAllBytes(GetSavePath);
            var restoredObject = SerializationUtility.DeserializeValue<Dictionary<Type, IPersistenceState>>(readBytes, DataFormat.JSON);
            InjectData(restoredObject);
        }
    }
}