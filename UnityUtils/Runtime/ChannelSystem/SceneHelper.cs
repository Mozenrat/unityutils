﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Essentials.ChannelSystem
{
    public static class SceneHelper
    {
        public static AsyncOperation LoadScene(SceneReference sceneReference, Action onDone = null, bool setNewSceneAsActive = true)
        {
            var asyncOperation = SceneManager.LoadSceneAsync(sceneReference, LoadSceneMode.Additive);
            asyncOperation.completed += _ =>
            {
                if (setNewSceneAsActive)
                {
                    var loadedScene = SceneManager.GetSceneByPath(sceneReference);
                    SceneManager.SetActiveScene(loadedScene);
                }
                onDone?.Invoke();
            };

            return asyncOperation;
        }
        
        public static AsyncOperation UnloadScene(SceneReference sceneReference, Action onDone = null)
        {
            return UnloadScene(sceneReference.ScenePath, onDone);
        }
        
        public static AsyncOperation UnloadScene(string sceneReference, Action onDone = null)
        {
            var asyncOperation = SceneManager.UnloadSceneAsync(sceneReference);
            if (asyncOperation != null) asyncOperation.completed += _ => onDone?.Invoke();

            return asyncOperation;
        }

        public static bool IsSceneLoaded(SceneReference sceneReference)
        {
            return SceneManager.GetSceneByPath(sceneReference).isLoaded;
        }
        
        public static async Awaitable UnloadAllScenesExcept(Action onSceneUnloaded = null, Action onDone = null, params SceneReference[] sceneReferences)
        {
            for (var i = SceneManager.sceneCount - 1; i >= 0; i--)
            {
                var scene = SceneManager.GetSceneAt(i);
                var shouldUnload = true;
                foreach (var sceneReference in sceneReferences)
                {
                    if (scene.path == sceneReference)
                    {
                        shouldUnload = false;
                        break;
                    }
                }

                if (shouldUnload)
                {
                    var handle = UnloadScene(scene.path, onSceneUnloaded);
                    if (handle == null) continue;
                    await Awaitable.FromAsyncOperation(handle);
                    Awaitable.NextFrameAsync();
                }
            }
            
            onDone?.Invoke();
        }
    }
}