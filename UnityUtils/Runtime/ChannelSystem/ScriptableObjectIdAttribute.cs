using UnityEngine;

namespace Essentials.ChannelSystem
{
    public class ScriptableObjectIdAttribute : PropertyAttribute { }
}