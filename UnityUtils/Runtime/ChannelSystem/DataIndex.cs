﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Essentials.ChannelSystem
{
    /// <summary>
    /// Global data index used at runtime to reference event channels and data objects
    /// Make sure that addressables are built and correct labels exist
    /// </summary>
    [CreateAssetMenu(menuName = "ChannelSystem/DataIndex")]
    public class DataIndex : ScriptableObject
    {
        private static HashSet<ScriptableObject> _channelIndex;
        private static Dictionary<string, ScriptableObject> _dataIndex;
        private static Dictionary<string, ScriptableObject> _dataIndexById;

        private const string DataObjectLabel = "DataObject";
        private const string ChannelObjectLabel = "ChannelObject";

        public static T GetChannel<T>() where T : ScriptableObject
        {
            var channelObject = _channelIndex.SingleOrDefault(item => item.GetType() == typeof(T)) as T;
            if (channelObject == null) Debug.LogError($"Channel object of type: {typeof(T)} not found in index!");
            return channelObject;
        }

        public static T GetDataAsset<T>(string dataObjectName) where T : ScriptableObject
        {
            if (_dataIndex.TryGetValue(dataObjectName, out var dataObject))
                return dataObject as T;
            Debug.LogError($"Data object with name: {dataObjectName} not found in index!");
            return null;
        }
        
        public static T GetDataAssetById<T>(string dataObjectId) where T : ScriptableObject
        {
            var dataObject = _dataIndexById[dataObjectId] as T;
            if (dataObject == null) Debug.LogError($"Data object with id: {dataObjectId} not found in index!");
            return dataObject;
        }
        
        public static List<T> GetAllDataAssetsOfType<T>() where T : ScriptableObject
        {
            return _dataIndex.Values.OfType<T>().ToList();
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Init()
        {
            if (_dataIndex != null && _dataIndexById != null) return;
            if (!DataObjectDataExists())
            {
                Debug.LogWarning("Addressables might not been built correctly or 'DataObject' labels are not set correctly!");
            }
            else
            {
                var dataObjectsLoadHandle = Addressables.LoadAssetsAsync<ScriptableObject>(DataObjectLabel, null);
                var dataObjectList = dataObjectsLoadHandle.WaitForCompletion();
                _dataIndex = new Dictionary<string, ScriptableObject>();
                if (dataObjectList != null && dataObjectList.Count > 0)
                {
                    foreach (var dataObject in dataObjectList)
                    {
                        _dataIndex.Add(dataObject.name, dataObject);
                    }
                }
            
                _dataIndexById = new Dictionary<string, ScriptableObject>();
                if (dataObjectList != null && dataObjectList.Count > 0)
                {
                    foreach (var dataObject in dataObjectList)
                    {
                        // find fields that are decorated with the [ScriptableObjectId] attribute and index them by their value
                        dataObject
                            .GetType()
                            .GetFields()
                            .Where(field => field.GetCustomAttributes(typeof(ScriptableObjectIdAttribute), false).Length > 0)
                            .ToList()
                            .ForEach(field => _dataIndexById.Add(field.GetValue(dataObject).ToString(), dataObject));
                    }
                }
            }

            if (_channelIndex != null) return;
            if (!ChannelDataExists())
            {
                Debug.LogWarning("Addressables might not been built correctly or 'ChannelObject' labels are not set correctly!");
            }
            else
            {
                var channelObjectsLoadHandle = Addressables.LoadAssetsAsync<ScriptableObject>(ChannelObjectLabel, null);
                var channelObjectList = channelObjectsLoadHandle.WaitForCompletion();
                _channelIndex = new HashSet<ScriptableObject>();
                if (channelObjectList != null && channelObjectList.Count > 0)
                {
                    foreach (var channelObject in channelObjectList)
                    {
                        _channelIndex.Add(channelObject);
                    }
                }
            }
        }
        
        private static bool DataObjectDataExists()
        {
            var dataObjectsHandle = Addressables.LoadResourceLocationsAsync(DataObjectLabel).WaitForCompletion();
            return dataObjectsHandle.Count > 0;
        }
        
        private static bool ChannelDataExists()
        {
            var channelObjectsHandle = Addressables.LoadResourceLocationsAsync(ChannelObjectLabel).WaitForCompletion();
            return channelObjectsHandle.Count > 0;
        }
    }
}