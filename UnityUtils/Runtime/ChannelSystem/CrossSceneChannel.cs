﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Essentials.ChannelSystem
{
    [CreateAssetMenu(menuName = "ChannelSystem/CrossSceneChannel")]
    public class CrossSceneChannel : ScriptableObject
    {
        private readonly Dictionary<Type, Behaviour> objectCache = new Dictionary<Type, Behaviour>();

        public void RegisterObject(Type objectType, Behaviour monoBehaviour)
        {
            if (objectCache.ContainsKey(objectType)) {
                Debug.LogWarning($"Object of type {objectType} is already registered in CrossSceneChannel, overwriting.");
                objectCache[objectType] = monoBehaviour;
                return;
            }
            objectCache.Add(objectType, monoBehaviour);
        }

        public void UnregisterObject(Type objectType)
        {
            objectCache.Remove(objectType);
        }

        public T GetReference<T>() where T : class
        {
            if (objectCache.TryGetValue(typeof(T), out var gameObject)) {
                return gameObject as T;
            }

            return null;
        }
    }
}