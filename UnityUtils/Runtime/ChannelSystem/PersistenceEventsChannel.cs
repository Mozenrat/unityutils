﻿using System;
using System.Collections.Generic;
using Essentials.Persistence;
using UnityEngine;

namespace Essentials.ChannelSystem
{
    [CreateAssetMenu(menuName = "ChannelSystem/PersistenceEventsChannel")]
    public class PersistenceEventsChannel : ScriptableObject
    {
        /// <summary>
        /// Subscribe to this event in every class that needs to save some kind of data
        /// The class must inherit from IPersistenceObject
        /// </summary>
        public event Action OnGatherData;
        public event EventHandler<PersistenceEventArgs> OnSendData;

        public event Action OnSaveGameData;
        public event Action OnLoadGameData;

        public void InvokeOnGatherData()
        {
            OnGatherData?.Invoke();
        }
        
        public void InvokeOnSendData(object sender, PersistenceEventArgs eventArgs)
        {
            OnSendData?.Invoke(sender, eventArgs);
        }

        public void InvokeSaveGameData()
        {
            OnSaveGameData?.Invoke();
        }
        
        public void InvokeLoadGameData()
        {
            OnLoadGameData?.Invoke();
        }
    }
    
    public class PersistenceEventArgs
    {
        public KeyValuePair<Type, IPersistenceState> PersistenceObject;

        public PersistenceEventArgs(KeyValuePair<Type, IPersistenceState> persistenceObject)
        {
            PersistenceObject = persistenceObject;
        }
    }
}