﻿using System.Collections.Generic;
using Essentials.ProceduralHexGrid.Internal;
using UnityEngine;

namespace Essentials.ProceduralHexGrid
{
    public class HexGridMap : MonoBehaviour
    {
        [SerializeField] private Material HexGridMaterial;
        [SerializeField] private HexCellRenderer HexCellPrefab;

        [Header("Grid settings")]
        [SerializeField] private bool Is2DGrid;
        [SerializeField] private Vector2Int GridSize;
        [SerializeField] private bool IsFlatTopped;
        
        [Header("Tile settings")]
        [SerializeField] private float HexInnerSize;
        [SerializeField] private float HexOuterSize;
        [SerializeField] private float HexHeight;

        private List<GameObject> existingGridTiles;
        private HexCellData cellSettings;

        private void Awake()
        {
            existingGridTiles = new List<GameObject>();
            cellSettings = new HexCellData(HexGridMaterial, Is2DGrid, HexInnerSize, HexOuterSize, HexHeight, IsFlatTopped);
        }

        private void OnValidate()
        {
            cellSettings = new HexCellData(HexGridMaterial, Is2DGrid, HexInnerSize, HexOuterSize, HexHeight, IsFlatTopped);
        }

        public void DrawGridMap()
        {
            foreach (var tile in existingGridTiles)
            {
                Destroy(tile);
            }
            existingGridTiles.Clear();
            
            for (int x = 0; x < GridSize.x; x++)
            {
                for (int y = 0; y < GridSize.y; y++)
                {
                    var hexTilePos = CalcHexPositionFromCoordinate(x, y);
                    var hexTile = Instantiate(HexCellPrefab, hexTilePos, Quaternion.identity, transform);
                    hexTile.name = $"HexTile: {x}, {y}";
                    hexTile.Init(cellSettings);
                    
                    existingGridTiles.Add(hexTile.gameObject);
                }
            }
        }

        private Vector3 CalcHexPositionFromCoordinate(int inputXPosition, int inputYPosition)
        {
            var column = inputXPosition;
            var row = inputYPosition;

            float width;
            float height;
            float xPos;
            float yPos;
            bool shouldOffset;
            float horizontalDistance;
            float verticalDistance;
            float offset;
            float size = HexOuterSize;

            if (!IsFlatTopped)
            {
                shouldOffset = row % 2 == 0;
                width = 1.73205f * size; // Sqrt(3)
                height = 2f * size;

                horizontalDistance = width;
                verticalDistance = height * .75f;
                offset = shouldOffset ? width * .5f : 0f;

                xPos = column * horizontalDistance + offset;
                yPos = row * verticalDistance;
            }
            else
            {
                shouldOffset = column % 2 == 0;
                width = 2f * size;
                height = 1.73205f * size; // Sqrt(3)

                horizontalDistance = width * .75f;
                verticalDistance = height;
                offset = shouldOffset ? height * .5f : 0f;
                
                xPos = column * horizontalDistance;
                yPos = row * verticalDistance - offset;
            }

            return new Vector3(xPos, 0f, -yPos);
        }
    }
}