﻿using UnityEngine;

namespace Essentials.ProceduralHexGrid.Internal
{
    public struct HexCellData
    {
        public Material HexGridMaterial;
        public bool Is2DGrid;
        public float HexInnerSize;
        public float HexOuterSize;
        public float HexHeight;
        public bool IsFlatTopped;

        public HexCellData(Material hexGridMaterial, bool is2DGrid, float hexInnerSize, float hexOuterSize, float hexHeight, bool isFlatTopped)
        {
            HexGridMaterial = hexGridMaterial;
            Is2DGrid = is2DGrid;
            HexInnerSize = hexInnerSize;
            HexOuterSize = hexOuterSize;
            HexHeight = hexHeight;
            IsFlatTopped = isFlatTopped;
        }
    }
}