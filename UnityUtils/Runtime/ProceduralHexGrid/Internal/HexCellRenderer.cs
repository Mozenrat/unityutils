﻿using System.Collections.Generic;
using UnityEngine;

namespace Essentials.ProceduralHexGrid.Internal
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class HexCellRenderer : MonoBehaviour
    {
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;
        private Mesh mesh;

        private HexCellData cellData;
        private List<GeometryFace> faces;

        public void Init(HexCellData inputCellData)
        {
            cellData = inputCellData;
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();

            mesh = new Mesh();
            mesh.name = "HexCell";

            meshFilter.mesh = mesh;
            meshRenderer.material = cellData.HexGridMaterial;
            
            DrawCellMesh();
        }

        private void DrawCellMesh()
        {
            if (cellData.Is2DGrid)
            {
                DrawFaces2DOnly();
            }
            else
            {
                DrawFaces3D();
            }
            
            CombineFaces();
        }
        
        private void DrawFaces3D()
        {
            faces = new List<GeometryFace>();
            
            // Top faces
            for (int vertIndex = 0; vertIndex < 6; vertIndex++)
            {
                faces.Add(CreateFace(
                    cellData.HexInnerSize,
                    cellData.HexOuterSize,
                    cellData.HexHeight * .5f,
                    cellData.HexHeight * .5f,
                    vertIndex));
            }
            
            // Bottom faces
            for (int vertIndex = 0; vertIndex < 6; vertIndex++)
            {
                faces.Add(CreateFace(
                    cellData.HexInnerSize,
                    cellData.HexOuterSize, 
                    -cellData.HexHeight * .5f, 
                    -cellData.HexHeight * .5f,
                    vertIndex,
                    true));
            }
            
            // Outer side faces
            for (int vertIndex = 0; vertIndex < 6; vertIndex++)
            {
                faces.Add(CreateFace(
                    cellData.HexOuterSize,
                    cellData.HexOuterSize,
                    cellData.HexHeight * .5f,
                    -cellData.HexHeight * .5f,
                    vertIndex,
                    true));
            }
            
            // Inner side faces
            for (int vertIndex = 0; vertIndex < 6; vertIndex++)
            {
                faces.Add(CreateFace(
                    cellData.HexInnerSize,
                    cellData.HexInnerSize,
                    cellData.HexHeight * .5f,
                    -cellData.HexHeight * .5f,
                    vertIndex));
            }
        }

        private void DrawFaces2DOnly()
        {
            faces = new List<GeometryFace>();
            
            // Top faces only
            for (int vertIndex = 0; vertIndex < 6; vertIndex++)
            {
                faces.Add(CreateFace(
                    cellData.HexInnerSize,
                    cellData.HexOuterSize,
                    cellData.HexHeight * .5f,
                    cellData.HexHeight * .5f,
                    vertIndex));
            }
        }

        private void CombineFaces()
        {
            var verts = new List<Vector3>();
            var tris = new List<int>();
            var uvs = new List<Vector2>();

            for (int i = 0; i < faces.Count; i++)
            {
                var currentFace = faces[i];
                verts.AddRange(currentFace.vertices);
                uvs.AddRange(currentFace.uvs);

                var trisOffset = 4 * i;
                for (var j = 0; j < currentFace.triangles.Count; j++)
                {
                    tris.Add(currentFace.triangles[j] + trisOffset);
                }
            }

            mesh.vertices = verts.ToArray();
            mesh.triangles = tris.ToArray();
            mesh.uv = uvs.ToArray();
            mesh.RecalculateBounds();
        }

        private GeometryFace CreateFace(
            float innerHexRadius,
            float outerHexRadius,
            float heightA,
            float heightB,
            int vertexIndex,
            bool reverse = false)
        {
            // filter vert indices so the last tri connects to first
            var vertA = CalcHexVertex(innerHexRadius, heightB, vertexIndex);
            var vertB = CalcHexVertex(innerHexRadius, heightB, vertexIndex < 5 ? vertexIndex + 1 : 0);
            var vertC = CalcHexVertex(outerHexRadius, heightA, vertexIndex < 5 ? vertexIndex + 1 : 0);
            var vertD = CalcHexVertex(outerHexRadius, heightA, vertexIndex);

            var verts = new List<Vector3> { vertA, vertB, vertC, vertD };
            var tris = new List<int> { 0, 1, 2, 2, 3, 0 };
            var uvs = new List<Vector2> { new(0, 0), new(1, 0), new(1, 1), new(0, 1) };
            if (reverse) verts.Reverse();

            return new GeometryFace(verts, tris, uvs);
        }

        private Vector3 CalcHexVertex(float hexSize, float hexHeight, int vertIndex)
        {
            var angleDeg = cellData.IsFlatTopped ? 60 * vertIndex : 60 * vertIndex - 30;
            var angleRad = Mathf.PI / 180f * angleDeg;
            return new Vector3(hexSize * Mathf.Cos(angleRad), hexHeight, hexSize * Mathf.Sin(angleRad));
        }
    }
}