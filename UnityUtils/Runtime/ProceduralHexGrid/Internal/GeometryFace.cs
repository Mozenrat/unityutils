﻿using System.Collections.Generic;
using UnityEngine;

namespace Essentials.ProceduralHexGrid.Internal
{
    public struct GeometryFace
    {
        public List<Vector3> vertices;
        public List<int> triangles;
        public List<Vector2> uvs;

        public GeometryFace(List<Vector3> inputVertices, List<int> inputTriangles, List<Vector2> inputUvs)
        {
            vertices = inputVertices;
            triangles = inputTriangles;
            uvs = inputUvs;
        }
    }
}