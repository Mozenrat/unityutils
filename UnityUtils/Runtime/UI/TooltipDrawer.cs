﻿using System;
using System.Text;
using Essentials.TimerSystem;
using TMPro;
using UnityEngine;

namespace Essentials.UI
{
    public class TooltipDrawer : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI TooltipText; // actual text component
        [SerializeField] private RectTransform CanvasRectTransform; // root canvas
        [SerializeField] private RectTransform TooltipBackgroundTransform; // tooltip background
        [SerializeField] private RectTransform UiRootTransform; // Ui root
        [SerializeField] private Camera TooltipCamera; // Ui camera
        [SerializeField] private GameObject TooltipGraphic; // Parent of tooltip background and text
        [SerializeField] private bool useOverlayCanvas;

        private static TooltipDrawer _instance;

        private RectTransform graphicRectTransform;
        private Func<StringBuilder, string> stringFunc;

        private TooltipProvider activeTooltipProvider;
        private StringBuilder stringBuilder;

        private float updateContentPeriodCurrent = 0f;
        private float updateContentPeriodMax = 0.25f;

        public static void ShowTooltip_Static(Func<StringBuilder, string> text, TooltipProvider activeProvider)
        {
            if (text == null) return;
            _instance.ShowTooltip(text, activeProvider);
        }

        public static void HideTooltip_Static()
        {
            _instance.HideTooltip();
        }

        private void Awake()
        {
            if (_instance == null) _instance = this;
            TooltipText.autoSizeTextContainer = true;
            graphicRectTransform = TooltipGraphic.GetComponent<RectTransform>();
            TooltipGraphic.SetActive(false);
            stringBuilder = new StringBuilder();
        }

        private void Update()
        {
            if (!TooltipGraphic.activeSelf) return;

            updateContentPeriodCurrent += Time.deltaTime;
            if (updateContentPeriodCurrent >= updateContentPeriodMax)
            {
                UpdateTooltipContent();
                updateContentPeriodCurrent = 0f;
            }

            CalculateTooltipPosition();
        }

        private void CalculateTooltipPosition()
        {
            if (!activeTooltipProvider || !activeTooltipProvider.gameObject.activeInHierarchy)
            {
                HideTooltip();
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                UiRootTransform,
                Input.mousePosition,
                useOverlayCanvas ? null : TooltipCamera,
                out var localPoint);

            TooltipGraphic.transform.localPosition = localPoint + new Vector2(20, -20 - TooltipText.preferredHeight);
            var anchoredPosition = graphicRectTransform.anchoredPosition;

            if (anchoredPosition.x + TooltipBackgroundTransform.rect.width > CanvasRectTransform.rect.width)
            {
                anchoredPosition.x = CanvasRectTransform.rect.width - TooltipBackgroundTransform.rect.width;
            }

            if (anchoredPosition.y + TooltipBackgroundTransform.rect.height < TooltipBackgroundTransform.rect.height)
            {
                anchoredPosition.y = 0;
            }

            graphicRectTransform.anchoredPosition = anchoredPosition;
        }

        private void UpdateTooltipContent()
        {
            TooltipText.SetText(stringFunc(stringBuilder));
            Vector2 backgroundSize = new Vector2(TooltipText.preferredWidth, TooltipText.preferredHeight);
            TooltipBackgroundTransform.sizeDelta = backgroundSize;
        }

        private void ShowTooltip(Func<StringBuilder, string> tooltipText, TooltipProvider provider)
        {
            stringFunc = tooltipText;
            activeTooltipProvider = provider;
            TooltipGraphic.SetActive(true);

            UpdateTooltipContent();
        }

        private void HideTooltip()
        {
            stringFunc = null;
            TooltipGraphic.SetActive(false);
        }
    }
}