﻿using TMPro;
using UnityEngine;

namespace Essentials.UI
{
    public class CameraFacingTextComponent : MonoBehaviour
    {
        private Camera targetCam;
        private TextMeshPro thisLabel;

        private void Awake()
        {
            targetCam = Camera.main;
            thisLabel = GetComponent<TextMeshPro>();
        }

        private void LateUpdate()
        {
            transform.forward = targetCam.transform.forward;
        }

        public void SetText(string label)
        {
            thisLabel.SetText(label);
        }
    }
}