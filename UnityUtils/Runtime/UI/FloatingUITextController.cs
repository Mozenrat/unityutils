﻿// using System;
// using UnityEngine;
//
// namespace Essentials.UI
// {
//     public class FloatingUITextController : MonoBehaviour
//     {
//         private static FloatingUITextController _instance;
//         
//         [SerializeField] private Camera Camera;
//         [SerializeField] private Canvas Canvas;
//         [SerializeField] private FloatingUITextStill FloatingTextStillPrefab;
//         [SerializeField] private FloatingUITextMoving FloatingTextMovingPrefab;
//         
//         private void Awake()
//         {
//             if (_instance == null) _instance = this;
//         }
//         
//         public static void WriteNotMoving(string text, Vector3 worldPos, float duration = 2f, Action onDone = null)
//         {
//             var screenPos = _instance.Camera.WorldToScreenPoint(worldPos);
//             var floatingText = Instantiate(_instance.FloatingTextStillPrefab, _instance.Canvas.transform);
//             floatingText.GetComponent<RectTransform>().anchoredPosition3D = screenPos;
//             floatingText.SetupNotMoving(text, duration, onDone);
//         }
//         
//         public static void WriteMoving(string text, GameObject trackedObject, float duration = 2f, Vector3 movement = default, Action onDone = null, Color backgroundColor = default, Color textColor = default)
//         {
//             var screenPos = _instance.Camera.WorldToScreenPoint(trackedObject.transform.position);
//             var floatingText = Instantiate(_instance.FloatingTextMovingPrefab, _instance.Canvas.transform);
//             floatingText.GetComponent<RectTransform>().anchoredPosition3D = screenPos;
//             floatingText.SetupMoving(text, _instance.Camera, trackedObject.transform, duration, movement, onDone, backgroundColor, textColor);
//         }
//     }
// }