﻿using System.Text;

namespace Essentials.UI
{
    public interface ITooltipProvider
    {
        public string GetTooltipString(StringBuilder stringBuilder);
    }
}