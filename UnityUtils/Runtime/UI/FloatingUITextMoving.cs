﻿// using System;
// using DG.Tweening;
// using Essentials.TimerSystem;
// using TMPro;
// using UnityEngine;
// using UnityEngine.UI;
//
// namespace Essentials.UI
// {
//     public class FloatingUITextMoving : MonoBehaviour
//     {
//         [SerializeField] private TextMeshProUGUI TextLabel;
//         [SerializeField] private RectTransform RectTransform;
//         [SerializeField] private RectTransform TweenableBackground;
//         
//         [SerializeField] private Image BackgroundImage;
//         
//         [SerializeField] private Color DefaultBackgroundColor;
//         [SerializeField] private Color DefaultTextColor;
//         
//         private Color backgroundColor;
//         private Color textColor;
//         private Vector3 positionShift;
//         
//         private Camera trackingCamera;
//         private Transform trackedObject;
//         private Sequence tween;
//
//         private void Update()
//         {
//             if (tween == null || trackedObject == null) return;
//             UpdateCurrentPosition();
//         }
//
//         public void SetupMoving(string text, Camera trackingCamera, Transform trackedObject, float duration = 2f, Vector3 floatingShift = default, Action onTweenComplete = null, Color backgroundColor = default, Color textColor = default)
//         {
//             if (onTweenComplete == null) onTweenComplete = () => Destroy(gameObject);
//             this.trackedObject = trackedObject;
//             this.trackingCamera = trackingCamera;
//
//             positionShift = floatingShift != default ? floatingShift : (Vector3.up + Vector3.right) * 40f;
//             this.backgroundColor = backgroundColor != default ? backgroundColor : DefaultBackgroundColor;
//             this.textColor = textColor != default ? textColor : DefaultTextColor;
//
//             SetupText(text);
//             
//             TimerHost.ExecuteOnceNextFrameTimer(() =>
//             {
//                 //TODO: this only supports moving to the right and up
//                 tween = DOTween.Sequence();
//                 tween.Append(TweenableBackground.DOAnchorPos3D(TweenableBackground.anchoredPosition3D + positionShift, duration));
//                 tween.AppendCallback(onTweenComplete.Invoke);
//             });
//         }
//
//         private void UpdateCurrentPosition()
//         {
//             var screenPos = trackingCamera.WorldToScreenPoint(trackedObject.position);
//             RectTransform.anchoredPosition3D = screenPos;
//         }
//         
//         private void SetupText(string text)
//         {
//             TextLabel.text = text;
//             TextLabel.color = textColor;
//             BackgroundImage.color = backgroundColor;
//             Vector2 backgroundSize = new Vector2(TextLabel.preferredWidth, TextLabel.preferredHeight);
//             TweenableBackground.sizeDelta = backgroundSize;
//         }
//     }
// }