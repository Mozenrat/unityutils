﻿// using System;
// using DG.Tweening;
// using TMPro;
// using UnityEngine;
//
// namespace Essentials.UI
// {
//     public class FloatingUITextStill : MonoBehaviour
//     {
//         [SerializeField] private TextMeshProUGUI TextLabel;
//         [SerializeField] private RectTransform RectTransform;
//         
//         private readonly Vector3 positionShift = (Vector3.up + Vector3.right) * 40f;
//         
//         public void SetupNotMoving(string text, float duration = 2f, Action onTweenComplete = null)
//         {
//             if (onTweenComplete == null) onTweenComplete = () => Destroy(gameObject);
//             
//             SetupText(text);
//             
//             var currentPos = RectTransform.anchoredPosition3D;
//             var tween = DOTween.Sequence();
//             tween.Append(RectTransform.DOAnchorPos3D(currentPos + positionShift, duration));
//             tween.AppendCallback(onTweenComplete.Invoke);
//         }
//         
//         private void SetupText(string text)
//         {
//             TextLabel.text = text;
//             Vector2 backgroundSize = new Vector2(TextLabel.preferredWidth, TextLabel.preferredHeight);
//             RectTransform.sizeDelta = backgroundSize;
//         }
//     }
// }