﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Essentials.UI
{
    /// <summary>
    /// Add this component and call InitTooltip in Start/Awake to have tooltip on it
    /// Also implement ITooltipProvider on same class
    /// </summary>
    public class TooltipProvider : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private Func<StringBuilder, string> getTooltipFunc;

        public void InitTooltip(ITooltipProvider iProvider)
        {
            getTooltipFunc = iProvider.GetTooltipString;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            TooltipDrawer.ShowTooltip_Static(getTooltipFunc, this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            TooltipDrawer.HideTooltip_Static();
        }
    }
}